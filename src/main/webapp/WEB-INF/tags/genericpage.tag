<%@tag pageEncoding="UTF-8" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags" %>

<html>
<head>
    <meta charset="utf-8">
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Ogrenci Bilgi ve Yönetim Portalı</title>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.0.12/css/all.css">
</head>
<body>

<nav class="navbar navbar-inverse" role="banner">
    <div class="container">

        <div class="navbar-header">

            <button class="navbar-toggle" type="button" data-toggle="collapse" data-target=".navbar-collapse">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>

            <a href="/" class="navbar-brand">Öğrenci Bilgi Sistemi</a>
        </div>

        <nav class="collapse navbar-collapse" role="navigation">


            <ul class="nav navbar-nav navbar-right">
                <li><a href="/">Anasayfa</a></li>
                <sec:authorize var="loggedIn" access="isAuthenticated()"/>
                <c:choose>
                    <c:when test="${loggedIn}">
                        <li>
                            <script>
                                function formSubmit() {
                                    document.getElementById("logoutForm").submit();
                                }
                            </script>

                            <a href="javascript:formSubmit()">Çıkış Yap</a>
                        </li>
                    </c:when>
                    <c:otherwise>
                        <li>
                            <a href="/login">Giriş Yap</a>
                        </li>
                    </c:otherwise>
                </c:choose>
            </ul>

        </nav>

    </div>
</nav>

<form action="<c:url value="/logout" />" method="POST" id="logoutForm">
    <input type="hidden" name="${_csrf.parameterName}"
           value="${_csrf.token}"/>
</form>

<div id="body">
    <jsp:doBody/>
</div>

<script src="https://code.jquery.com/jquery-3.1.1.min.js"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
</body>
</html>