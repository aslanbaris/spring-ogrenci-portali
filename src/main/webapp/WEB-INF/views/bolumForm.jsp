<%@ page pageEncoding="UTF-8" contentType="text/html; charset=UTF-8" %>
<%@ taglib prefix="t" tagdir="/WEB-INF/tags" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>


<t:genericpage>
    <div class="container">
        <div class="row">
            <div class="col-md-4 col-md-offset-4">

                <a href="/admin/bolum" class="btn btn-primary">Bütün Bölümler</a>

                <h3>Bölüm Formu</h3>


                <form:form acceptCharset="UTF-8" method="POST" modelAttribute="bolumForm" action="/admin/bolum">
                    <form:input type="hidden" path="id" id="id"/>

                    <div class="form-group">
                        <form:input cssClass="form-control" placeholder="Bölüm Adı" path="adi" id="adi"/>
                        <form:errors path="adi" cssClass="control-label" />
                    </div>

                    <div class="form-group">
                        <form:select path="fakulte.id" class="form-control">
                            <form:option value="-1" label="-- Fakülte Seçiniz --"/>
                            <form:options items="${fakulteList}" itemValue="id" itemLabel="adi"/>
                        </form:select>
                        <form:errors path="fakulte.id" cssClass="control-label" />
                    </div>

                    <input type="submit" name="submit" class="btn btn-primary" value="Kaydet">
                </form:form>
            </div>
        </div>
    </div>
</t:genericpage>
