<%@ page pageEncoding="UTF-8" contentType="text/html; charset=UTF-8" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="t" tagdir="/WEB-INF/tags" %>


<t:genericpage>
    <div class="container">
        <div class="row">
            <div class="col-md-4 col-md-offset-4">

                <c:if test="${not empty msg}">
                    <div class="alert alert-info alert-dismissible" role="alert">
                        <button type="button" class="close" data-dismiss="alert"
                                aria-label="Close"><span
                                aria-hidden="true">&times;</span></button>
                            ${msg}
                    </div>
                </c:if>

                <div class="panel panel-info">
                    <div class="panel-heading">
                        <div class="panel-title">Giriş Yap</div>
                    </div>

                    <div style="padding-top:30px" class="panel-body">

                        <c:if test="${not empty error}">
                            <div class="alert alert-danger alert-dismissible" role="alert">
                                <button type="button" class="close" data-dismiss="alert"
                                        aria-label="Close"><span
                                        aria-hidden="true">&times;</span></button>
                                <strong>Hata!</strong> ${error}
                            </div>
                        </c:if>

                        <form action="<c:url value='/login' />" method="POST" class="form-horizontal"
                              role="form">

                            <div style="margin-bottom: 25px" class="input-group">
                                <span class="input-group-addon"><i class="glyphicon glyphicon-user"></i></span>
                                <input type="text" name="username" class="form-control" id="username"
                                       placeholder="Kullanıcı Adı">
                            </div>

                            <div style="margin-bottom: 25px" class="input-group">
                                <span class="input-group-addon"><i class="glyphicon glyphicon-lock"></i></span>
                                <input type="password" name="password" class="form-control" id="password"
                                       placeholder="Parola">
                            </div>


                            <div style="margin-top:10px" class="form-group">
                                <div class="col-sm-12 controls">
                                    <input type="submit" name="submit" class="btn btn-primary" value="Giriş Yap">
                                    <input type="hidden" name="${_csrf.parameterName}" value="${_csrf.token}"/>
                                </div>
                            </div>

                        </form>


                    </div>
                </div>
            </div>

        </div>
    </div>

</t:genericpage>