<%@ page pageEncoding="UTF-8" contentType="text/html; charset=UTF-8" %>
<%@ taglib prefix="t" tagdir="/WEB-INF/tags" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>


<t:genericpage>
    <div class="container">
        <div class="row">
            <div class="col-md-4 col-md-offset-4">

                <h3 style="color: #428bca">İşlemler:</h3>
                <hr>

                <div class="row">
                    <div class="col-md-4">
                        <a class="btn btn-info" href="/admin/ogrenci">Öğrenci</a>
                    </div>
                    <div class="col-md-4">
                        <a class="btn btn-info" href="/admin/bolum">Bölüm</a> <br>
                    </div>
                    <div class="col-md-4">
                        <a class="btn btn-info" href="/admin/fakulte">Fakülte</a> <br>
                    </div>
                </div>

            </div>
        </div>
    </div>
</t:genericpage>