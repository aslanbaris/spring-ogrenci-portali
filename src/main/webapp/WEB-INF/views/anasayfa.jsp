<%@ page pageEncoding="UTF-8" contentType="text/html; charset=UTF-8" %>
<%@ taglib prefix="t" tagdir="/WEB-INF/tags" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags" %>


<t:genericpage>
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-8 col-md-offset-2">

                <h3>Öğrenci Bilgi Sistemi</h3>
                <hr>

                <sec:authorize var="loggedIn" access="isAuthenticated()"/>
                <c:choose>
                    <c:when test="${loggedIn}">
                        <sec:authorize access="hasRole('ADMIN')">
                            <a class="btn btn-primary" href="/admin/">Yönetici Sayfası</a>
                        </sec:authorize>
                        <sec:authorize access="hasRole('OGRENCI')">
                            <a class="btn btn-primary" href="/ogrenci/<sec:authentication property="principal.username" />">Profil Sayfam</a>
                        </sec:authorize>
                    </c:when>
                    <c:otherwise>
                        <a class="btn btn-primary" href="/login">Giriş Yap</a>
                    </c:otherwise>
                </c:choose>
            </div>
        </div>
    </div>
</t:genericpage>