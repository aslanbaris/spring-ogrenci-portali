<%@ page pageEncoding="UTF-8" contentType="text/html; charset=UTF-8" %>
<%@ taglib prefix="t" tagdir="/WEB-INF/tags" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>


<t:genericpage>
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-8 col-md-offset-2">

                <a class="btn btn-info"  href="/admin/">Geri Dön</a>

                <div class="row">
                    <div class="col-md-4">
                        <h3>Öğrenciler</h3>
                    </div>
                    <div class="col-md-2 col-md-offset-6">
                        <a class="btn btn-info"  href="/admin/ogrenci/ekle">Öğrenci Ekle</a>
                    </div>
                </div>


                <table class="table table-striped">
                    <tr class="koyu_mavi">
                        <th>Öğrenci No</th>
                        <th>Adı</th>
                        <th>Soyadı</th>
                        <th>Doğum Tarihi</th>
                        <th>Sınıfı</th>
                        <th>Öğrenim Grubu</th>
                        <th>Bölümü</th>
                        <th>İşlem</th>
                    </tr>
                    <c:forEach var="ogrenci" items="${ogrenciler}">
                        <tr>
                            <td>${ogrenci.ogrenciNo}</td>
                            <td>${ogrenci.adi}</td>
                            <td>${ogrenci.soyadi}</td>
                            <td><fmt:formatDate pattern="yyyy/MM/dd" value="${ogrenci.dogumTarihi}"/></td>
                            <td>${ogrenci.sinifi == 0 ? "Hazırlık" : ogrenci.sinifi}</td>
                            <td>${ogrenci.ogrenimGrubu == 0 ? "N.Ö" : "İ.Ö"}</td>
                            <td>${ogrenci.bolum.adi}</td>
                            <td>
                                <a href="/admin/ogrenci/${ogrenci.id}/guncelle" class="btn btn-primary">Güncelle</a>
                                <a href="/admin/ogrenci/${ogrenci.id}/sil" class="btn btn-danger">Sil</a>
                            </td>
                        </tr>
                    </c:forEach>

                </table>
                <br/>

            </div>
        </div>
    </div>
</t:genericpage>