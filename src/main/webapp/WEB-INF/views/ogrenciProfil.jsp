<%@ page pageEncoding="UTF-8" contentType="text/html; charset=UTF-8" %>
<%@ taglib prefix="t" tagdir="/WEB-INF/tags" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>


<t:genericpage>
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-6 col-md-offset-3">

                <h3>Öğrenci Profil Sayfası</h3>
                <br>

                <div class="thumbnail">
                    <div class="caption">
                        <p><strong>Ad Soyad:</strong> ${ogrenci.adi} ${ogrenci.soyadi}</p>
                        <p><strong>Öğrenci No:</strong> ${ogrenci.ogrenciNo}</p>
                        <p><strong>Doğum Tarihi:</strong> <fmt:formatDate pattern="yyyy/MM/dd" value="${ogrenci.dogumTarihi}"/></p>
                        <p><strong>Sınıfı:</strong> ${ogrenci.sinifi == 0 ? "Hazırlık" : ogrenci.sinifi} Sınıf</p>
                        <p><strong>Öğrenim Grubu:</strong> ${ogrenci.ogrenimGrubu == 0 ? "N.Ö" : "İ.Ö"}</p>
                        <p><strong>Bölümü:</strong> ${ogrenci.bolum.adi}</p>
                        <p><strong>Fakültesi:</strong> ${ogrenci.bolum.fakulte.adi}</p>
                        <p><strong>Adresi:</strong> ${ogrenci.adres}</p>
                    </div>
                </div>

            </div>
        </div>
    </div>
</t:genericpage>