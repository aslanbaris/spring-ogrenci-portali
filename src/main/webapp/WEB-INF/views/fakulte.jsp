<%@ page pageEncoding="UTF-8" contentType="text/html; charset=UTF-8" %>
<%@ taglib prefix="t" tagdir="/WEB-INF/tags" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>


<t:genericpage>
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-8 col-md-offset-2">

                <a class="btn btn-info"  href="/admin/">Geri Dön</a>

                <div class="row">
                    <div class="col-md-4">
                        <h3>Fakülteler</h3>
                    </div>
                    <div class="col-md-2 col-md-offset-6">
                        <a class="btn btn-info"  href="/admin/fakulte/ekle">Fakülte Ekle</a>
                    </div>
                </div>


                <table class="table table-striped">
                    <tr class="koyu_mavi">
                        <th>Fakülte Adı</th>
                        <th>İşlem</th>
                    </tr>
                    <c:forEach var="fakulte" items="${fakulteler}">
                        <tr>
                            <td>${fakulte.adi}</td>
                            <td>
                                <a href="/admin/fakulte/${fakulte.id}/guncelle" class="btn btn-primary">Güncelle</a>
                                <a href="/admin/fakulte/${fakulte.id}/sil" class="btn btn-danger">Sil</a>
                            </td>
                        </tr>
                    </c:forEach>

                </table>
                <br/>

            </div>
        </div>
    </div>
</t:genericpage>