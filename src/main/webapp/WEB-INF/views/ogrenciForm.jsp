<%@ page import="java.util.Date" %>
<%@ page pageEncoding="UTF-8" contentType="text/html; charset=UTF-8" %>
<%@ taglib prefix="t" tagdir="/WEB-INF/tags" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jstl/fmt" %>

<t:genericpage>

    <div class="container">
        <div class="row">
            <div class="col-md-4 col-md-offset-4">

                <a href="/admin/ogrenci" class="btn btn-primary">Bütün Öğrenciler</a>

                <h3>Öğrenci Formu</h3>


                <form:form acceptCharset="UTF-8" method="POST" modelAttribute="ogrenciForm" action="/admin/ogrenci" >
                    <form:input type="hidden" path="id" id="id"/>

                    <div class="form-group">
                        <form:input cssClass="form-control" placeholder="Öğrenci Kullanıcı Adı" path="user.username" id="user.username"/>
                        <form:errors path="user.username" cssClass="control-label" />
                    </div>

                    <div class="form-group">
                        <form:input cssClass="form-control" placeholder="Öğrenci No" path="ogrenciNo" id="ogrenciNo"/>
                        <form:errors path="ogrenciNo" cssClass="control-label" />
                    </div>

                    <div class="form-group">
                        <form:input cssClass="form-control" placeholder="Ad" path="adi" id="adi"/>
                        <form:errors path="adi" cssClass="control-label" />
                    </div>

                    <div class="form-group">
                        <form:input cssClass="form-control" placeholder="Soyad" path="soyadi" id="soyadi"/>
                        <form:errors path="soyadi" cssClass="control-label" />
                    </div>

                    <div class="form-group">
                        <form:input cssClass="form-control" placeholder="Doğum Tarihi" path="dogumTarihi"
                                    id="dogumTarihi"/>
                        <form:errors path="dogumTarihi" cssClass="control-label" />
                    </div>

                    <div class="form-group">
                        <form:textarea cssClass="form-control" path="adres" rows="2" placeholder="Adres" />
                        <form:errors path="adres" cssClass="control-label" />
                    </div>

                    <div class="form-group">
                        <form:select path="sinifi" class="form-control">
                            <form:option value="-1" label="-- Sınıf Seçiniz --"/>
                            <form:options items="${sinifList}"/>
                        </form:select>
                        <form:errors path="sinifi" cssClass="control-label" />
                    </div>

                    <div class="form-group">
                        <form:radiobutton path="ogrenimGrubu" value="0" checked="true" /> Normal Öğretim
                        <form:radiobutton path="ogrenimGrubu" value="1" /> İkinci Öğretim
                    </div>

                    <div class="form-group">
                        <form:select path="bolum.id" class="form-control">
                            <form:option value="-1" label="-- Bölüm Seçiniz --"/>
                            <form:options items="${bolumList}" itemValue="id" itemLabel="adi"/>
                        </form:select>
                        <form:errors path="bolum.id" cssClass="control-label" />
                    </div>


                    <input type="submit" name="submit" class="btn btn-primary" value="Kaydet">
                </form:form>
            </div>
        </div>
    </div>
</t:genericpage>
