DROP DATABASE IF EXISTS OgrenciPortali;
CREATE DATABASE IF NOT EXISTS OgrenciPortali;
USE OgrenciPortali;

CREATE  TABLE USERS (
  username VARCHAR(45) NOT NULL ,
  password VARCHAR(60) NOT NULL ,
  enabled TINYINT NOT NULL DEFAULT 1 ,
  PRIMARY KEY (username));

CREATE TABLE USER_ROLES (
  user_role_id int(11) NOT NULL AUTO_INCREMENT,
  username varchar(45) NOT NULL,
  role varchar(45) NOT NULL,
  PRIMARY KEY (user_role_id),
  UNIQUE KEY uni_username_role (role,username),
  KEY fk_username_idx (username),
  CONSTRAINT fk_username FOREIGN KEY (username) REFERENCES USERS (username)
);

CREATE TABLE IF NOT EXISTS Fakulte (
  `fakulte_id` INTEGER AUTO_INCREMENT NOT NULL,
  `adi` NVARCHAR(255) NOT NULL,
  PRIMARY KEY (`fakulte_id`)
);

CREATE TABLE IF NOT EXISTS Bolum (
  `bolum_id` INTEGER AUTO_INCREMENT NOT NULL,
  `fakulte_id` INTEGER NOT NULL,
  `adi` NVARCHAR(255) NOT NULL,
  PRIMARY KEY (`bolum_id`),
  FOREIGN KEY (fakulte_id) REFERENCES Fakulte(fakulte_id)
);

CREATE TABLE IF NOT EXISTS Ogrenci (
  `ogrenci_id` INTEGER AUTO_INCREMENT NOT NULL,
  `bolum_id` INTEGER NOT NULL,
  `username` VARCHAR(45) NOT NULL,
  `ogrenci_no` INTEGER NOT NULL,
  `adi` NVARCHAR(100) NOT NULL,
  `soyadi` NVARCHAR(100) NOT NULL,
  `dogum_tarihi` DATE NOT NULL,
  `adres` NVARCHAR(255) NOT NULL,
  `sinifi` INTEGER NOT NULL,
  `ogrenim_grubu` TINYINT NOT NULL,
  PRIMARY KEY (`ogrenci_id`),
  FOREIGN KEY (bolum_id) REFERENCES Bolum(bolum_id),
  CONSTRAINT fk_username_ogrenci FOREIGN KEY (username) REFERENCES USERS (username)
);


INSERT INTO USERS(username,password,enabled)
VALUES ('admin','$2a$10$nfYQyi8voBuM7eebF2bkSeJqafGTTWuTdHX7HgBOxqj8g3DOdJ7yW', true);

INSERT INTO USER_ROLES(username, role)
VALUES ('admin', 'ROLE_ADMIN');

INSERT INTO USERS(username,password,enabled)
VALUES ('ogrenci','$2a$10$nfYQyi8voBuM7eebF2bkSeJqafGTTWuTdHX7HgBOxqj8g3DOdJ7yW', true);

INSERT INTO USER_ROLES(username, role)
VALUES ('ogrenci', 'ROLE_OGRENCI');

INSERT INTO Fakulte(adi)
VALUES ('Mühendislik Fakültesi');

INSERT INTO Fakulte(adi)
VALUES ('Fen-Edebiyat Fakültesi');

INSERT INTO Bolum(fakulte_id, adi)
VALUES (1, 'Bilgisayar Mühendisliği');

INSERT INTO Bolum(fakulte_id, adi)
VALUES (1, 'Elektronik Mühendisliği');

INSERT INTO Bolum(fakulte_id, adi)
VALUES (1, 'Makina Mühendisliği');

SELECT * FROM Fakulte;
SELECT * FROM Bolum;
SELECT * FROM Ogrenci;