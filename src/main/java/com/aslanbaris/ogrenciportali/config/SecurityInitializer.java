package com.aslanbaris.ogrenciportali.config;

import org.springframework.security.web.context.AbstractSecurityWebApplicationInitializer;

public class SecurityInitializer
        extends AbstractSecurityWebApplicationInitializer {
}