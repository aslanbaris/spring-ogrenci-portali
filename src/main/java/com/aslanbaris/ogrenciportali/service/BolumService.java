package com.aslanbaris.ogrenciportali.service;

import com.aslanbaris.ogrenciportali.dao.BolumDAO;
import com.aslanbaris.ogrenciportali.model.Bolum;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Service("bolumService")
@Transactional(propagation = Propagation.REQUIRED, rollbackFor = Exception.class)
public class BolumService {

    @Autowired
    private BolumDAO dao;

    public void saveOrUpdateBolum(Bolum bolum) {
        dao.saveOrUpdateObject(bolum);
    }

    public boolean removeBolum(Bolum bolum) {
        return dao.removeObject(bolum);
    }


    public List<Bolum> loadBolumler() {
        return dao.loadBolumler();
    }

    public Bolum loadBolumById(Long id) {
        return dao.loadBolumById(id);
    }

}
