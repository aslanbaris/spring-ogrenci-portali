package com.aslanbaris.ogrenciportali.service;

import com.aslanbaris.ogrenciportali.dao.FakulteDAO;
import com.aslanbaris.ogrenciportali.model.Fakulte;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Service("fakulteService")
@Transactional(propagation = Propagation.REQUIRED, rollbackFor = Exception.class)
public class FakulteService {

    @Autowired
    private FakulteDAO dao;

    public void saveOrUpdateFakulte(Fakulte fakulte) {
        dao.saveOrUpdateObject(fakulte);
    }

    public boolean removeFakulte(Fakulte fakulte) {
        return dao.removeObject(fakulte);
    }

    public List<Fakulte> loadFakulteler() {
        return dao.loadFakulteler();
    }

    public Fakulte loadFakulteById(Long id) {
        return dao.loadFakulteById(id);
    }

}
