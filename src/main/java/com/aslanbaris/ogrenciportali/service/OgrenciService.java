package com.aslanbaris.ogrenciportali.service;

import com.aslanbaris.ogrenciportali.dao.OgrenciDAO;
import com.aslanbaris.ogrenciportali.dao.UserDAO;
import com.aslanbaris.ogrenciportali.dao.UserRoleDAO;
import com.aslanbaris.ogrenciportali.model.Ogrenci;
import com.aslanbaris.ogrenciportali.model.User;
import com.aslanbaris.ogrenciportali.model.UserRole;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Service("ogrenciService")
@Transactional(propagation = Propagation.REQUIRED, rollbackFor = Exception.class)
public class OgrenciService {

    @Autowired
    private OgrenciDAO dao;

    @Autowired
    private UserDAO userDAO;

    @Autowired
    private UserRoleDAO userRoleDAO;

    public void saveOrUpdateOgrenci(Ogrenci ogrenci) {
        String username = ogrenci.getUser().getUsername();
        User user = userDAO.findByUserName(username);

        if (user == null) {
            String parola = new BCryptPasswordEncoder().encode("123");
            user = new User(username, parola, true);
            userDAO.saveOrUpdateObject(user);

            UserRole role = new UserRole();
            role.setUser(user);
            role.setRole("ROLE_OGRENCI");
            userRoleDAO.saveOrUpdateObject(role);

            ogrenci.setUser(user);
        }

        dao.saveOrUpdateObject(ogrenci);
    }

    public boolean removeOgrenci(Ogrenci ogrenci) {
        return dao.removeObject(ogrenci);
    }

    public List<Ogrenci> loadOgrenciler() {
        return dao.loadOgrenciler();
    }

    public Ogrenci loadOgrenciById(Long id) {
        return dao.loadOgrenciById(id);
    }

    public Ogrenci loadOgrenciByUsername(String username) {
        return dao.loadOgrenciByUsername(username);
    }
}
