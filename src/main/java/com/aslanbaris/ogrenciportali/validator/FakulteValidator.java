package com.aslanbaris.ogrenciportali.validator;

import com.aslanbaris.ogrenciportali.model.Fakulte;
import org.springframework.stereotype.Component;
import org.springframework.validation.Errors;
import org.springframework.validation.ValidationUtils;
import org.springframework.validation.Validator;

@Component
public class FakulteValidator implements Validator {
    
    @Override
    public boolean supports(Class<?> clazz) {
        return Fakulte.class.equals(clazz);
    }

    @Override
    public void validate(Object object, Errors errors) {
        ValidationUtils.rejectIfEmpty(errors, "adi", "fakulte.adi.empty");
    }
}
