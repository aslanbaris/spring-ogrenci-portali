package com.aslanbaris.ogrenciportali.validator;

import com.aslanbaris.ogrenciportali.model.Ogrenci;
import org.springframework.stereotype.Component;
import org.springframework.validation.Errors;
import org.springframework.validation.ValidationUtils;
import org.springframework.validation.Validator;

@Component
public class OgrenciValidator implements Validator {
    
    @Override
    public boolean supports(Class<?> clazz) {
        return Ogrenci.class.equals(clazz);
    }

    @Override
    public void validate(Object object, Errors errors) {
        ValidationUtils.rejectIfEmpty(errors, "ogrenciNo", "ogrenci.ogrenciNo.empty");
        ValidationUtils.rejectIfEmpty(errors, "adi", "ogrenci.adi.empty");
        ValidationUtils.rejectIfEmpty(errors, "soyadi", "ogrenci.soyadi.empty");
        ValidationUtils.rejectIfEmpty(errors, "dogumTarihi", "ogrenci.dogumTarihi.empty");
        ValidationUtils.rejectIfEmpty(errors, "adres", "ogrenci.adres.empty");

        Ogrenci ogrenci = (Ogrenci) object;
        if (ogrenci.getSinifi() == -1) {
            errors.rejectValue("sinifi", "ogrenci.sinifi.empty");
        }
        if (ogrenci.getBolum().getId() == -1) {
            errors.rejectValue("bolum.id", "ogrenci.bolum.empty");
        }

    }
}
