package com.aslanbaris.ogrenciportali.validator;

import com.aslanbaris.ogrenciportali.model.Bolum;
import org.springframework.stereotype.Component;
import org.springframework.validation.Errors;
import org.springframework.validation.ValidationUtils;
import org.springframework.validation.Validator;

@Component
public class BolumValidator implements Validator {
    
    @Override
    public boolean supports(Class<?> clazz) {
        return Bolum.class.equals(clazz);
    }

    @Override
    public void validate(Object object, Errors errors) {
        ValidationUtils.rejectIfEmpty(errors, "adi", "bolum.adi.empty");

        Bolum bolum = (Bolum) object;
        if (bolum.getFakulte().getId() == -1) {
            errors.rejectValue("fakulte.id", "bolum.fakulte.empty");
        }
    }
}
