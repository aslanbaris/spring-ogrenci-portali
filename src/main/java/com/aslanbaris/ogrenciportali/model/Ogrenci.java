package com.aslanbaris.ogrenciportali.model;

import org.hibernate.annotations.OnDelete;
import org.hibernate.annotations.OnDeleteAction;
import org.springframework.format.annotation.DateTimeFormat;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;

@Entity
@Table(name = "Ogrenci")
public class Ogrenci implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "ogrenci_id", nullable = false)
    private Long id;

    @Column(name = "ogrenci_no", nullable = false)
    private Long ogrenciNo;

    @Column(name = "adi", nullable = false)
    private String adi;

    @Column(name = "soyadi", nullable = false)
    private String soyadi;

    @DateTimeFormat(pattern = "yyyy/MM/dd")
    @Column(name = "dogum_tarihi", nullable = false)
    private Date dogumTarihi;

    @Column(name = "adres", nullable = false)
    private String adres;

    @Column(name = "sinifi", nullable = false)
    private Integer sinifi;

    @Column(name = "ogrenim_grubu", nullable = false)
    private Integer ogrenimGrubu;

    @ManyToOne
    @OnDelete(action = OnDeleteAction.CASCADE)
    @JoinColumn(name = "bolum_id", referencedColumnName = "bolum_id", nullable = false)
    private Bolum bolum;

    @OneToOne
    @JoinColumn(name = "username", referencedColumnName = "username", nullable = false)
    private User user;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getOgrenciNo() {
        return ogrenciNo;
    }

    public void setOgrenciNo(Long ogrenciNo) {
        this.ogrenciNo = ogrenciNo;
    }

    public String getAdi() {
        return adi;
    }

    public void setAdi(String adi) {
        this.adi = adi;
    }

    public String getSoyadi() {
        return soyadi;
    }

    public void setSoyadi(String soyadi) {
        this.soyadi = soyadi;
    }

    public Date getDogumTarihi() {
        return dogumTarihi;
    }

    public void setDogumTarihi(Date dogumTarihi) {
        this.dogumTarihi = dogumTarihi;
    }

    public String getAdres() {
        return adres;
    }

    public void setAdres(String adres) {
        this.adres = adres;
    }

    public Integer getSinifi() {
        return sinifi;
    }

    public void setSinifi(Integer sinifi) {
        this.sinifi = sinifi;
    }

    public Integer getOgrenimGrubu() {
        return ogrenimGrubu;
    }

    public void setOgrenimGrubu(Integer ogrenimGrubu) {
        this.ogrenimGrubu = ogrenimGrubu;
    }

    public Bolum getBolum() {
        return bolum;
    }

    public void setBolum(Bolum bolum) {
        this.bolum = bolum;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }
}
