package com.aslanbaris.ogrenciportali.model;

import javax.persistence.*;
import java.io.Serializable;

@Entity
@Table(name = "Fakulte")
public class Fakulte implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "fakulte_id", nullable = false)
    private Long id;

    @Column(name = "adi", nullable = false)
    private String adi;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getAdi() {
        return adi;
    }

    public void setAdi(String adi) {
        this.adi = adi;
    }

}
