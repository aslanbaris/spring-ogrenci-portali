package com.aslanbaris.ogrenciportali.dao;

import com.aslanbaris.ogrenciportali.model.Fakulte;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.query.Query;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;
import java.util.List;

@Repository
public class FakulteDAO {

    @Autowired
    private SessionFactory sessionFactory;

    public Session getCurrentSession() {
        return sessionFactory.getCurrentSession();
    }

    public boolean saveOrUpdateObject(Object object) {
        getCurrentSession().saveOrUpdate(object);
        return true;
    }

    public boolean removeObject(Fakulte fakulte) {
        getCurrentSession().remove(fakulte);
        return true;
    }

    public List<Fakulte> loadFakulteler() {
        Session currentSession = getCurrentSession();
        CriteriaBuilder criteriaBuilder = currentSession.getCriteriaBuilder();
        CriteriaQuery<Fakulte> criteriaQuery = criteriaBuilder.createQuery(Fakulte.class);
        Root<Fakulte> root = criteriaQuery.from(Fakulte.class);
        criteriaQuery.select(root);

        Query<Fakulte> query = currentSession.createQuery(criteriaQuery);

        List<Fakulte> fakulteList = query.getResultList();
        return fakulteList;
    }

    public Fakulte loadFakulteById(Long id) {
        Session currentSession = getCurrentSession();
        CriteriaBuilder criteriaBuilder = currentSession.getCriteriaBuilder();
        CriteriaQuery<Fakulte> criteriaQuery = criteriaBuilder.createQuery(Fakulte.class);
        Root<Fakulte> root = criteriaQuery.from(Fakulte.class);

        Predicate predicateURL = criteriaBuilder.equal(root.get("id"), id);
        criteriaQuery.select(root).where(predicateURL);
        criteriaQuery.distinct(true);

        Query<Fakulte> query = currentSession.createQuery(criteriaQuery);
        Fakulte fakulte = query.getSingleResult();
        return fakulte;
    }
    
}
