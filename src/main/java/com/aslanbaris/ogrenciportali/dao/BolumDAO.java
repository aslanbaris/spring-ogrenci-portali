package com.aslanbaris.ogrenciportali.dao;

import com.aslanbaris.ogrenciportali.model.Bolum;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.query.Query;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;
import java.util.List;

@Repository
public class BolumDAO {

    @Autowired
    private SessionFactory sessionFactory;

    public Session getCurrentSession() {
        return sessionFactory.getCurrentSession();
    }

    public boolean saveOrUpdateObject(Object object) {
        getCurrentSession().saveOrUpdate(object);
        return true;
    }

    public boolean removeObject(Bolum bolum) {
        getCurrentSession().remove(bolum);
        return true;
    }

    public List<Bolum> loadBolumler() {
        Session currentSession = getCurrentSession();
        CriteriaBuilder criteriaBuilder = currentSession.getCriteriaBuilder();
        CriteriaQuery<Bolum> criteriaQuery = criteriaBuilder.createQuery(Bolum.class);
        Root<Bolum> root = criteriaQuery.from(Bolum.class);
        criteriaQuery.select(root);

        Query<Bolum> query = currentSession.createQuery(criteriaQuery);

        List<Bolum> bolumList = query.getResultList();
        return bolumList;
    }

    public Bolum loadBolumById(Long id) {
        Session currentSession = getCurrentSession();
        CriteriaBuilder criteriaBuilder = currentSession.getCriteriaBuilder();
        CriteriaQuery<Bolum> criteriaQuery = criteriaBuilder.createQuery(Bolum.class);
        Root<Bolum> root = criteriaQuery.from(Bolum.class);

        Predicate predicateURL = criteriaBuilder.equal(root.get("id"), id);
        criteriaQuery.select(root).where(predicateURL);
        criteriaQuery.distinct(true);

        Query<Bolum> query = currentSession.createQuery(criteriaQuery);
        Bolum bolum = query.getSingleResult();
        return bolum;
    }
    
}
