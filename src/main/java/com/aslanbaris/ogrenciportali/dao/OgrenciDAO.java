package com.aslanbaris.ogrenciportali.dao;

import com.aslanbaris.ogrenciportali.model.Ogrenci;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.query.Query;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;
import java.util.List;

@Repository
public class OgrenciDAO {

    @Autowired
    private SessionFactory sessionFactory;

    public Session getCurrentSession() {
        return sessionFactory.getCurrentSession();
    }

    public boolean saveOrUpdateObject(Ogrenci ogrenci) {
        getCurrentSession().saveOrUpdate(ogrenci);
        return true;
    }

    public boolean removeObject(Ogrenci ogrenci) {
        getCurrentSession().remove(ogrenci);
        return true;
    }

    public List<Ogrenci> loadOgrenciler() {
        Session currentSession = getCurrentSession();
        CriteriaBuilder criteriaBuilder = currentSession.getCriteriaBuilder();
        CriteriaQuery<Ogrenci> criteriaQuery = criteriaBuilder.createQuery(Ogrenci.class);
        Root<Ogrenci> root = criteriaQuery.from(Ogrenci.class);
        criteriaQuery.select(root);

        Query<Ogrenci> query = currentSession.createQuery(criteriaQuery);

        List<Ogrenci> ogrenciList = query.getResultList();
        return ogrenciList;
    }

    public Ogrenci loadOgrenciById(Long id) {
        Session currentSession = getCurrentSession();
        CriteriaBuilder criteriaBuilder = currentSession.getCriteriaBuilder();
        CriteriaQuery<Ogrenci> criteriaQuery = criteriaBuilder.createQuery(Ogrenci.class);
        Root<Ogrenci> root = criteriaQuery.from(Ogrenci.class);

        Predicate predicateURL = criteriaBuilder.equal(root.get("id"), id);
        criteriaQuery.select(root).where(predicateURL);
        criteriaQuery.distinct(true);

        Query<Ogrenci> query = currentSession.createQuery(criteriaQuery);
        Ogrenci ogrenci = query.getSingleResult();
        return ogrenci;
    }

    public Ogrenci loadOgrenciByUsername(String username) {
        Session currentSession = getCurrentSession();
        CriteriaBuilder criteriaBuilder = currentSession.getCriteriaBuilder();
        CriteriaQuery<Ogrenci> criteriaQuery = criteriaBuilder.createQuery(Ogrenci.class);
        Root<Ogrenci> root = criteriaQuery.from(Ogrenci.class);

        Predicate predicateURL = criteriaBuilder.equal(root.get("user").get("username"), username);
        criteriaQuery.select(root).where(predicateURL);
        criteriaQuery.distinct(true);

        Query<Ogrenci> query = currentSession.createQuery(criteriaQuery);
        Ogrenci ogrenci = query.getSingleResult();
        return ogrenci;
    }

}
