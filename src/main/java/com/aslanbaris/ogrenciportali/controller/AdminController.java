package com.aslanbaris.ogrenciportali.controller;

import com.aslanbaris.ogrenciportali.model.Bolum;
import com.aslanbaris.ogrenciportali.model.Fakulte;
import com.aslanbaris.ogrenciportali.model.Ogrenci;
import com.aslanbaris.ogrenciportali.service.BolumService;
import com.aslanbaris.ogrenciportali.service.FakulteService;
import com.aslanbaris.ogrenciportali.service.OgrenciService;
import com.aslanbaris.ogrenciportali.validator.BolumValidator;
import com.aslanbaris.ogrenciportali.validator.FakulteValidator;
import com.aslanbaris.ogrenciportali.validator.OgrenciValidator;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.validation.BindingResult;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.*;

import java.util.LinkedHashMap;
import java.util.Map;

@Controller
@RequestMapping(value = "/admin/*")
public class AdminController {

    @Autowired
    private OgrenciService ogrenciService;

    @Autowired
    private BolumService bolumService;

    @Autowired
    private FakulteService fakulteService;

    @Autowired
    private OgrenciValidator ogrenciValidator;

    @Autowired
    private BolumValidator bolumValidator;

    @Autowired
    private FakulteValidator fakulteValidator;

    @InitBinder("ogrenciForm")
    protected void initOgrenciBinder(WebDataBinder binder) {
        binder.addValidators(ogrenciValidator);
    }

    @InitBinder("bolumForm")
    protected void initBolumBinder(WebDataBinder binder) {
        binder.addValidators(bolumValidator);
    }

    @InitBinder("fakulteForm")
    protected void initFakulteBinder(WebDataBinder binder) {
        binder.addValidators(fakulteValidator);
    }

    @RequestMapping(value = "/", method = RequestMethod.GET)
    public String adminHome() {

        return "admin";
    }

    @RequestMapping(value = "/ogrenci", method = RequestMethod.GET)
    public String ogrenci(ModelMap model) {

        model.addAttribute("ogrenciler", ogrenciService.loadOgrenciler());

        return "ogrenci";
    }

    @RequestMapping(value = "/ogrenci/ekle", method = RequestMethod.GET)
    public String ogrenciEkle(ModelMap model) {

        Ogrenci ogrenci = new Ogrenci();
        model.addAttribute("ogrenciForm", ogrenci);
        fillOgrenciModel(model);

        return "ogrenciForm";
    }

    @RequestMapping(value = "/ogrenci/{ogrId}/guncelle", method = RequestMethod.GET)
    public String ogrenciGuncelle(@PathVariable("ogrId") Long id, ModelMap model) {

        Ogrenci ogrenci = ogrenciService.loadOgrenciById(id);
        model.addAttribute("ogrenciForm", ogrenci);
        fillOgrenciModel(model);

        return "ogrenciForm";
    }

    @RequestMapping(value = "/ogrenci", method = RequestMethod.POST)
    public String ogrenciEkleVeyaGuncelle(@ModelAttribute("ogrenciForm") @Validated Ogrenci ogrenci,
                                          BindingResult result, ModelMap model)
    {

        if (result.hasErrors()) {
            fillOgrenciModel(model);
            return "ogrenciForm";
        }

        ogrenciService.saveOrUpdateOgrenci(ogrenci);

        return "redirect:/admin/ogrenci";
    }

    @RequestMapping(value = "/ogrenci/{ogrId}/sil", method = RequestMethod.GET)
    public String ogrenciSil(@PathVariable("ogrId") Long id) {

        Ogrenci ogrenci = ogrenciService.loadOgrenciById(id);
        ogrenciService.removeOgrenci(ogrenci);

        return "redirect:/admin/ogrenci";
    }

    @RequestMapping(value = "/fakulte", method = RequestMethod.GET)
    public String fakulte(ModelMap model) {

        model.addAttribute("fakulteler", fakulteService.loadFakulteler());

        return "fakulte";
    }

    @RequestMapping(value = "/fakulte/ekle", method = RequestMethod.GET)
    public String fakulteEkle(ModelMap model) {

        Fakulte fakulte = new Fakulte();
        model.addAttribute("fakulteForm", fakulte);

        return "fakulteForm";
    }

    @RequestMapping(value = "/fakulte/{fakId}/guncelle", method = RequestMethod.GET)
    public String fakulteGuncelle(@PathVariable("fakId") Long id, ModelMap model) {

        Fakulte fakulte = fakulteService.loadFakulteById(id);
        model.addAttribute("fakulteForm", fakulte);

        return "fakulteForm";
    }

    @RequestMapping(value = "/fakulte", method = RequestMethod.POST)
    public String fakulteEkleVeyaGuncelle(@ModelAttribute("fakulteForm") @Validated Fakulte fakulte,
                                          BindingResult result)
    {

        if (result.hasErrors()) {
            return "fakulteForm";
        }

        fakulteService.saveOrUpdateFakulte(fakulte);

        return "redirect:/admin/fakulte";
    }

    @RequestMapping(value = "/fakulte/{fakId}/sil", method = RequestMethod.GET)
    public String fakulteSil(@PathVariable("fakId") Long id) {

        Fakulte fakulte = fakulteService.loadFakulteById(id);
        fakulteService.removeFakulte(fakulte);

        return "redirect:/admin/fakulte";
    }

    @RequestMapping(value = "/bolum", method = RequestMethod.GET)
    public String bolum(ModelMap model) {

        model.addAttribute("bolumler", bolumService.loadBolumler());

        return "bolum";
    }

    @RequestMapping(value = "/bolum/ekle", method = RequestMethod.GET)
    public String bolumEkle(ModelMap model) {

        Bolum bolum = new Bolum();
        model.addAttribute("bolumForm", bolum);
        model.addAttribute("fakulteList", fakulteService.loadFakulteler());

        return "bolumForm";
    }

    @RequestMapping(value = "/bolum/{bolumId}/guncelle", method = RequestMethod.GET)
    public String bolumGuncelle(@PathVariable("bolumId") Long id, ModelMap model) {

        Bolum bolum = bolumService.loadBolumById(id);
        model.addAttribute("bolumForm", bolum);
        model.addAttribute("fakulteList", fakulteService.loadFakulteler());

        return "bolumForm";
    }

    @RequestMapping(value = "/bolum", method = RequestMethod.POST)
    public String bolumEkleVeyaGuncelle(@ModelAttribute("bolumForm") @Validated Bolum bolum,
                                        BindingResult result, ModelMap model)
    {
        if (result.hasErrors()) {
            model.addAttribute("fakulteList", fakulteService.loadFakulteler());
            return "bolumForm";
        }

        bolumService.saveOrUpdateBolum(bolum);

        return "redirect:/admin/bolum";
    }

    @RequestMapping(value = "/bolum/{bolumId}/sil", method = RequestMethod.GET)
    public String bolumSil(@PathVariable("bolumId") Long id) {

        Bolum bolum = bolumService.loadBolumById(id);
        bolumService.removeBolum(bolum);

        return "redirect:/admin/bolum";
    }

    private void fillOgrenciModel(ModelMap model) {

        Map<String, String> sinifList = new LinkedHashMap<String, String>();
        sinifList.put("0", "Hazırlık");
        sinifList.put("1", "1. Sınıf");
        sinifList.put("2", "2. Sınıf");
        sinifList.put("3", "3. Sınıf");
        sinifList.put("4", "4. Sınıf");
        model.addAttribute("sinifList", sinifList);

        model.addAttribute("bolumList", bolumService.loadBolumler());
    }

}
